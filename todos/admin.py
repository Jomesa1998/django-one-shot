from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
admin.site.register(TodoList)
admin.site.register(TodoItem)


class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")
