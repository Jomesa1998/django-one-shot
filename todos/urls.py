from django.urls import path
from todos.views import show_todo_list, todo_list_list

urlpatterns = [
    path("todos/", show_todo_list),
    path("", todo_list_list, name="todo_list_list"),
]
