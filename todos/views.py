from django.shortcuts import render
from todos.models import TodoList, TodoItem


# Create your views here.
def show_todo_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {"todos_object": todos}
    return render(request, "todos/detail.html", context)


def todo_list_list(request):
    todos = TodoItem.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/detail.html", context)
